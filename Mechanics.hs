module Mechanics where

import Characters

data Mechanics = Mechanics
  { suerte :: Int
  -- combate
  , iniciativa :: Int
  , def_combate :: Int
  , def_desprevenido :: Int
  , bonf_daño_cc :: Int
  , bonf_daño_dist :: Int
  -- cordura
  , entereza :: Int
  -- salud
  , aguante :: Int
  -- toxicity
  , toxicity :: Int

  } deriving Show

seeStats :: Character -> Mechanics
seeStats cha = Mechanics
  luck init def_comb def_flatfooted b_hit_cc b_hit_dist ent agu tox
  where
    chars = characs cha
    luck = vol chars `div` 2
    tox =  ((fort chars `div` 2) + vol chars) * 2
    agu = (fort chars) + (vol chars `div` 2)
    def_comb = (ref chars) + (acrobatics $ skills cha) + 5
    def_flatfooted = def_comb - 3
    init = (ref chars) + (int chars `div` 2)
    b_hit_cc = (fort chars + max (light_weapons $ skills cha) (heavy_weapons $ skills cha)) `div` 4
    b_hit_dist = (range_weapons $ skills cha) `div` 4
    ent = (vol chars) + ((int chars) `div` 2)
