function hitos(x) = median 3d10

\\ Trkth Stats:
\ Tdef := 23;
\ Tatt := 18;
Tinit := 9;
Thp := 21;

\ Bicho
\ Eatt := 15;
\ Edef := 15;
Einit := 9;
Ehp := 18;
tFirst := ( (call hitos(0)) + Tinit) > ((call hitos(0)) + Einit);

if tFirst then call trTkthHit(Thp, Ehp) else call trEHit(Thp, Ehp)

function trTkthHit(hpT, hpE) =
  Tatt := 18; Edef := 17;
  if hpT <= 0 then 0
  else 
  tirada := call hitos(0);
  if (tirada + Tatt) >= Edef then call trEHit(hpT, hpE - tirada)
  else call trEHit(hpT, hpE)

function trEHit(hpT, hpE) =
  Tdef := 23; Eatt := 17;
  if hpE <= 0 then 1
  else
  tirada := call hitos(0);
  if (tirada + Eatt) >= Tdef then call trTkthHit(hpT - tirada, hpE)
  else call trTkthHit(hpT, hpE)

