{-# Language DeriveGeneric #-}
{-# Language LambdaCase #-}
{-# Language OverloadedStrings #-}
module Characters where

import GHC.Generics
import Data.Text
import Data.Aeson
import Data.Aeson.Types

data Class = Rogue | Stupid deriving (Show,Generic)

instance FromJSON Class
instance ToJSON Class

data Race = Witcher | Idiot deriving (Show,Generic)

instance FromJSON Race
instance ToJSON Race


data Skills = Skills
  { -- Tipitco corriendo
    acrobatics :: Int
  , weight_lifting :: Int
  , runner :: Int
  , ride :: Int
  -- Puño cerrado?
  , heavy_weapons :: Int
  , light_weapons :: Int
  , range_weapons :: Int
  , artillery_weapons :: Int
  -- El ojo que todo lo ve
  , perception :: Int
  , investigation :: Int
  , tracking :: Int
  , detect_magic :: Int
  -- Mascarita?
  , stealth :: Int
  , sleight_of_hand :: Int
  , scape_artist :: Int
  , disable_device :: Int
  -- Conversation
  , persuade :: Int
  , sense_motive :: Int
  , intimidation :: Int
  , bluff :: Int
  -- Librito
  , local_knowledge :: Int
  , magic_knowledge :: Int
  , super_natural_knowledge :: Int
  , beast_knowledge :: Int
  , survival :: Int
  , craft :: Int
  -- Herramienta
  , trade :: Int
  , herborist :: Int
  , medic :: Int
  , blacksmith :: Int
  } deriving (Generic, Show)

-- instance FromJSON Skills
instance FromJSON Skills where
    parseJSON (Object v) = Skills 
        <$> ((v .:? "acrobatics") .!= 0) 
        <*> (v .:? "weight_lifting" .!= 0)
        <*> (v .:? "runner" .!= 0)
        <*> (v .:? "ride" .!= 0)
        <*> (v .:? "heavy_weapons" .!= 0) 
        <*> (v .:? "light_weapons" .!= 0)
        <*> (v .:? "range_weapons" .!= 0)
        <*> (v .:? "artillery_weapons" .!= 0)
        <*> (v .:? "perception" .!= 0)
        <*> (v .:? "investigation" .!= 0)
        <*> (v .:? "tracking" .!= 0) 
        <*> (v .:? "detect_magic" .!= 0)
        <*> (v .:? "stealth" .!= 0)
        <*> (v .:? "sleight_of_hand" .!= 0)
        <*> (v .:? "scape_artist" .!= 0)
        <*> (v .:? "disable_device" .!= 0)
        <*> (v .:? "persuade" .!= 0) 
        <*> (v .:? "sense_motive" .!= 0)
        <*> (v .:? "intimidation" .!= 0)
        <*> (v .:? "bluff" .!= 0)
        <*> (v .:? "local_knowledge" .!= 0)
        <*> (v .:? "magic_knowledge" .!= 0)
        <*> (v .:? "super_natural_knowledge" .!= 0)
        <*> (v .:? "beast_knowledge" .!= 0) 
        <*> (v .:? "survival" .!= 0)
        <*> (v .:? "craft" .!= 0)
        <*> (v .:? "trade" .!= 0)
        <*> (v .:? "herborist" .!= 0)
        <*> (v .:? "medic" .!= 0) 
        <*> (v .:? "blacksmith" .!= 0)
--
instance ToJSON Skills -- No needed

defSkill :: Skills
defSkill = Skills 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

-- instance FromJSON Skills where
--   parseJSON 

data Characteristics = Characteristic
  { fort :: Int
  , vol :: Int
  , ref :: Int
  , int :: Int
  } deriving (Generic, Show)

instance FromJSON Characteristics where
    parseJSON (Object v) = Characteristic
        <$> v .:? "fort" .!= 0
        <*> v .:? "vol" .!= 0
        <*> v .:? "ref" .!= 0
        <*> v .:? "int" .!= 0

instance ToJSON Characteristics -- No needed

defCharacs :: Characteristics
defCharacs = Characteristic 0 0 0 0

data Character = Character
  { name :: String
  , level :: Int
  , skills :: Skills
  , characs :: Characteristics
  , cls :: Class
  , race :: Race
  , dramaPoints :: Int
  } deriving (Generic, Show)

instance FromJSON Character where
    parseJSON (Object v) = Character 
        <$> v .:? "name" .!= "Bobisson"
        <*> v .:? "level" .!= 0
        <*> v .:? "skills" .!= defSkill
        <*> v .:? "characs" .!= defCharacs
        <*> v .:? "cls" .!=  Stupid
        <*> v .:? "race" .!=  Idiot
        <*> v .:? "dramaPoints" .!= 3
        
instance ToJSON Character

defCha :: Character
defCha = Character "" 0 defSkill defCharacs Stupid Idiot 3
