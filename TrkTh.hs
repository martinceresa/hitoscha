module Trkth where

import Characters
import Mechanics

trkthSkills :: Skills
trkthSkills = defSkill
  {acrobatics = 9
  , light_weapons = 9
  , range_weapons = 8
  , stealth = 9
  , sleight_of_hand = 9
  , disable_device = 5
  }

trkthCharacs :: Characteristics
trkthCharacs = defCharacs
  { fort = 5
  , ref = 9
  , vol = 4
  , int = 1}

trkth :: Character
trkth = defCha
        { name = "Trkth"
        , skills = trkthSkills
        , characs = trkthCharacs
        , cls = Rogue
        , race = Witcher
        , dramaPoints = 3
        }

trkmech :: Mechanics
trkmech = seeStats trkth
