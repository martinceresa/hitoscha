import Characters
import Mechanics

import Data.Aeson

import System.Environment
import qualified Data.ByteString.Lazy as B
main = do
  n : _ <- getArgs
  x <- B.readFile n
  print $ ((decode x) :: Maybe Character)

